// function fetchRandomNumbers(callback){
//     console.log('Fetching number...');
//     setTimeout(() => {
//         let randomNum = Math.floor(Math.random() * (100 - 0 + 1)) + 0;
//         console.log('Received random number:', randomNum);
//         callback(randomNum);
//     }, (Math.floor(Math.random() * (5)) + 1) * 1000);
// }

function fetchRandomNumbers(){
    return new Promise((resolve,reject)=> {
        if (err){
            reject(err);
        } else {

            console.log('Fetching number...');

            setTimeout(() => {
                let randomNum = Math.floor(Math.random() * (100 - 0 + 1)) + 0;
                console.log('Received random number:', randomNum);
                resolve(randomNum);
            }, (Math.floor(Math.random() * (5)) + 1) * 1000);

        }
        
    })
   
}

// function fetchRandomString(callback){
//     console.log('Fetching string...');
//     setTimeout(() => {
//         let result           = '';
//         let characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
//         let charactersLength = characters.length;
//         for ( let i = 0; i < 5; i++ ) {
//            result += characters.charAt(Math.floor(Math.random() * charactersLength));
//         }
//         console.log('Received random string:', result);
//         callback(result);
//     }, (Math.floor(Math.random() * (5)) + 1) * 1000);
// }

function fetchRandomString(){
    return new Promise((resolve,reject)=> {
        if (err){
            reject(err);
        } else {

            console.log('Fetching string...');

            setTimeout(() => {
            let result           = '';
            let characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
            let charactersLength = characters.length;

            for ( let i = 0; i < 5; i++ ) {
            result += characters.charAt(Math.floor(Math.random() * charactersLength));
            }

            console.log('Received random string:', result);
            resolve(result);
             }, (Math.floor(Math.random() * (5)) + 1) * 1000);

        }
        

    })
    
}



// fetchRandomNumbers((randomNum) => console.log(randomNum))
// fetchRandomString((randomStr) => console.log(randomStr))


function task2(){
    
    let sum = 0;

    fetchRandomNumbers()
    .then((n1)=> {return (n1+sum)})
    .then((sum1)=> {
        console.log("sum1 = ",sum1)
        fetchRandomNumbers().then((n2)=> {
            let sum2 = sum1 + n2;
            console.log("sum2 = ", sum2)})
    })
    .catch(function(e) {
        console.error(e.message); 
    });
}

// task2();


function task3(){

    Promise.all([fetchRandomNumbers(),fetchRandomString()])
    .then((arr)=> console.log(arr.join(" ")))
    .catch(function(e) {
        console.error(e.message); 
    });
}

// task3();

// Task 4: Fetch 10 random numbers simultaneously -> and print their sum.

function task4(){
    let n = 10;
    let arr = [];
    let i = 1;

    do { 
        
        arr.push(fetchRandomNumbers);
        i++;
        
    } while (i<=n)

    Promise.all(arr.map(f => f()))
    .then((arr) => {
        console.log(arr.reduce((acc,val)=> acc+val,0))
    })
    .catch(function(e) {
        console.error(e.message); 
    });
}
// task4();

// __________________________________________________
// Async Await tasks


async function asyncTask1_1(){
    try {

        const randomNum = await fetchRandomNumbers();
        console.log(randomNum);

    } catch (e) {
        console.error(e);
    }
    
}

// asyncTask1_1();


async function asyncTask1_2(){
    try {

        const randomNum = await fetchRandomString();
        console.log(randomNum);

    } catch (e) {
        console.error(e);
    }
    
}

// asyncTask1_2();

// Task 2: Fetch a random number -> add it to a sum variable and print sum-> fetch another random variable
// -> add it to the same sum variable and print the sum variable.

async function asyncTask2(){
    try {

        let sum = 0;
        const n1 = await fetchRandomNumbers();
        sum = sum + n1;
        console.log("sum = ", sum);

        const n2 = await fetchRandomNumbers();
        sum = sum + n2;
        console.log("sum = ", sum);

    } catch (err) {
        console.error(err);
    }
    
}   

// asyncTask2();

async function asyncTask3(){
    try {

        let arr = await Promise.all([fetchRandomNumbers(),fetchRandomString()]);
        console.log(arr.join(" "));

    } catch (e) {
        console.error(e);
    }
    
}

// asyncTask3();

async function asyncTask4(){
    try {
        let n = 10;
        let arr = [];
        let i = 1;

        do {
            arr.push(fetchRandomNumbers);
            i++;
        } while (i<=10)

        let result = await Promise.all(arr.map(f => f() ));
        console.log(result.reduce((acc,val) => acc+val,0))

    } catch (e) {
        console.error(e);
    }
    
}

// asyncTask4();