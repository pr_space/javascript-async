function getBoard() {
  return new Promise((resolve, reject) => {
    console.log('Fetching board...');
    setTimeout(function() {
      let board = {
        id: "def453ed",
        name: "Thanos"
      };
      console.log('Received board');
      resolve(board);
    }, 1000);
  });
}


function getLists(boardId) {
  return new Promise((resolve, reject) => {
    console.log(`Fetching lists for board id ${boardId}...`);
    setTimeout(function() {
      let lists = {
        def453ed: [
          {
            id: "qwsa221",
            name: "Mind"
          },
          {
            id: "jwkh245",
            name: "Space"
          },
          {
            id: "azxs123",
            name: "Soul"
          },
          {
            id: "cffv432",
            name: "Time"
          },
          {
            id: "ghnb768",
            name: "Power"
          },
          {
            id: "isks839",
            name: "Reality"
          }
        ]
      };
      console.log(`Received lists for board id ${boardId}`);
      resolve(lists[boardId]);
    }, 500);
  });
}

function getCards(listId) {
  return new Promise((resolve, reject) => {
    console.log(`Fetching cards for list id ${listId}...`);
    setTimeout(function() {
      let cards = {
        qwsa221: [
          {
            id: "1",
            description: `Having acquired the Power Stone, one of the six Infinity Stones,from the planet Xandar`
          },
          {
            id: "2",
            description: `Having acquired the Power Stone, one of the six Infinity Stones,from the planet Xandar`
          },
          {
            id: "3",
            description: `Having acquired the Power Stone, one of the six Infinity Stones,from the planet Xandar`
          }
        ],
        jwkh245: [
          {
            id: "1",
            description: `intercept a spaceship carrying the surviving Asgardians. As they extract the Space Stone from the Tesseract, Thanos subdues Thor, overpowers Hulk, and kills Heimdall and Loki.`
          },
          {
            id: "2",
            description: `intercept a spaceship carrying the surviving Asgardians. As they extract the Space Stone from the Tesseract, Thanos subdues Thor, overpowers Hulk, and kills Heimdall and Loki.`
          },
          {
            id: "3",
            description: `intercept a spaceship carrying the surviving Asgardians. As they extract the Space Stone from the Tesseract, Thanos subdues Thor, overpowers Hulk, and kills Heimdall and Loki.`
          },
          {
            id: "4",
            description: `intercept a spaceship carrying the surviving Asgardians. As they extract the Space Stone from the Tesseract, Thanos subdues Thor, overpowers Hulk, and kills Heimdall and Loki.`
          }
        ],
        azxs123: [
          {
            id: "1",
            description: `intercept a spaceship carrying the surviving Asgardians. As they extract the Space Stone from the Tesseract, Thanos subdues Thor, overpowers Hulk, and kills Heimdall and Loki.`
          },
          {
            id: "2",
            description: `intercept a spaceship carrying the surviving Asgardians. As they extract the Space Stone from the Tesseract, Thanos subdues Thor, overpowers Hulk, and kills Heimdall and Loki.`
          }
        ],
        cffv432: [
          {
            id: "1",
            description: `intercept a spaceship carrying the surviving Asgardians. As they extract the Space Stone from the Tesseract, Thanos subdues Thor, overpowers Hulk, and kills Heimdall and Loki.`
          },
          {
            id: "2",
            description: `intercept a spaceship carrying the surviving Asgardians. As they extract the Space Stone from the Tesseract, Thanos subdues Thor, overpowers Hulk, and kills Heimdall and Loki.`
          }
        ],
        ghnb768: [
          {
            id: "1",
            description: `intercept a spaceship carrying the surviving Asgardians. As they extract the Space Stone from the Tesseract, Thanos subdues Thor, overpowers Hulk, and kills Heimdall and Loki.`
          },
          {
            id: "2",
            description: `intercept a spaceship carrying the surviving Asgardians. As they extract the Space Stone from the Tesseract, Thanos subdues Thor, overpowers Hulk, and kills Heimdall and Loki.`
          }
        ]
      };
      console.log(`Received cards for list id ${listId}`);
      resolve(cards[listId] || []);
    }, 1500);
  });
}



function task1(listId1){

  getBoard()
  .then((board) => {
    return board.id
  })
  .then((boardId)=>{
    return getLists(boardId)
  })
  .then((arrObj)=>{
    return arrObj.filter((obj)=> obj['id']===listId1)
  })
  .then((obj)=>{
    return getCards(obj[0]['id'])
  })
  .then(console.log)
  .catch(function(e) {
    console.error(e.message); 
  });

}

// task1('qwsa221');


function task2(id1,id2){

  getBoard()
  .then((board)=>{
    return board.id
  })
  .then((id)=>{
    return getLists(id)
  })
  .then((arrObj)=>{
    return arrObj.filter(obj=> [id1,id2].includes(obj['id']))
  })
  .then((arrayObj)=>{
    
  return Promise.all([getCards(arrayObj[0]['id']),getCards(arrayObj[1]['id'])]);
  
  })
  .then((arr)=> {
    return arr.map(output => console.log(output))
  })
  .catch(function(e) {
    console.error(e.message); 
  });

}

// task2('qwsa221','jwkh245');

function task3(){

  getBoard()
  .then((board)=> {return board.id})
  .then ((id)=>{
    return getLists(id);
  })
  .then((arrObj)=> {
  
    let promises = [];
    
    arrObj.map(obj=> {
      promises.push(getCards(obj.id))
    })
    return Promise.all(promises)
  
  })
  .then((arr) => {
    arr.map(answer => console.log(answer));
  })
  .catch(function(e) {
    console.error(e.message); 
  });

}

// task3();

